# Aucun import ne doit être fait dans ce fichier

def nombre_entier(n: int) -> str: # n = 1 -> S0, n = 2 -> SS0, n = 3 -> SSS0, n = 1 -> S0, n = 4 -> SSSS0, etc..
    return "S" * n + "0"

def S(n: str) -> str: # n = "0" -> S0, n = "S0" -> SS0, n = "SS0" -> SSS0, n = "SSS0" -> SSSS0, n = "SSSS0" -> SSSSS0, etc..
    return "S" + n


def addition(a: str, b: str) -> str: # "0" + "SO" -> "S0", "SSSS0"(s = 4) + "SSSSSSSS0"(s = 8) -> "SSSSSSSSSSSS0"(s = 12), etc..
    if a == "0":
        return b
    elif b == "0":
        return a
    return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str: # "0" * "S0" -> "0", "SSSSS0"(s = 5) * "SSSSS0"(s = 5) -> "SSSSSSSSSSSSSSSSSSSSSSSSS0"(s = 25), etc..
    if a == "0":
        return "0"
    elif b == "0":
        return "0"
    return addition(multiplication(a[1:], b), b)


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)
    

def fibo_ite(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    
    a, b = 0, 1
    for _ in range(2, n + 1):
        a, b = b, a + b
    return b


def golden_phi(n: int) -> float:
    a = 1
    b = 1
    for i in range(n):
        a, b = b, a + b
    return b / a


def sqrt5(n: int) -> float:
    phi = golden_phi(n+1)
    return 2 * phi - 1

def pow(a: float, n: int) -> float:
    return a ** n
    